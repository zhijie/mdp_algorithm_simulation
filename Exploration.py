import time

from mapHandler import mapHandler
from robot import *
from astar import AStar


class Exploration:
    WIDTH = 15
    HEIGHT = 20
    SIZE = WIDTH * HEIGHT
    GOAL = 283

    class ExplorationStrategy(Enum):
        Left_Wall_Hug = 0
        Explore_Middle = 1

    class ModeOfExploration(Enum):
        Virtual_Run = 0
        Physical_Run = 1

    class Node:
        def __init__(self, index):
            self.index = index
            self.x = index % Exploration.WIDTH
            self.y = index // Exploration.WIDTH
            self.left = None
            self.forward = None
            self.right = None
            self.parent = None
            self.visited = False
            self.possible_moves = 0
            self.fully_explored = False

    def __init__(self, start_position=16,):

        # Init other modules
        self.robot = Robot(Robot.Orientation.North)
        self.mh = mapHandler()
        self.gui = None
        self.communication_module = None

        # Read maps
        self.current_map = list(str(self.mh.read_arena_file("sampleArena/ExploreArenaInitial.txt")))

        # Init variables for exploration
        self.exploration_strategy = Exploration.ExplorationStrategy.Left_Wall_Hug
        self.mode_of_exploration = None

        # Init variables
        self.explored_goal = False
        self.total_explored = 18  # self.start Grid & Goal Grid
        self.nodes = []
        self.time_limited_exploration = 0
        self.coverage_limited_exploration = 0
        self.steps_per_second = 0
        self.stop_exploration = False

        for j in range(self.mh.SIZE):
            self.nodes.append(Exploration.Node(j))
        self.start = self.nodes[start_position]

        # Update robot current position in map as explored with no obstacle
        robot = [self.start.index - 1, self.start.index, self.start.index + 1,
                 self.start.index + Exploration.WIDTH, self.start.index + Exploration.WIDTH + 1,
                 self.start.index + Exploration.WIDTH - 1,
                 self.start.index - Exploration.WIDTH, self.start.index - Exploration.WIDTH + 1,
                 self.start.index - Exploration.WIDTH - 1]
        for i in robot:
            self.current_map[i] = "0"

        self.sprint_moves = []
        self.sprint_start = None
        self.sprint_end = None
        self.sprint_orientation = None
        self.goal_in_sprint = False

    def init_virtual_exploration(self, gui, time_limited_exploration=0,
                                 coverage_limited_exploration=0, steps_per_second=1,
                                 obstacle_map_path=list(mapHandler.read_arena_file("sampleArena/arena2.txt"))):

        self.mode_of_exploration = Exploration.ModeOfExploration.Virtual_Run
        self.time_limited_exploration = int(time_limited_exploration)
        self.coverage_limited_exploration = int(coverage_limited_exploration)
        self.steps_per_second = 1 / int(steps_per_second)
        self.obstacle_map = obstacle_map_path
        self.gui = gui

    def init_physical_exploration(self, communication):
        self.mode_of_exploration = Exploration.ModeOfExploration.Physical_Run
        self.communication_module = communication
        self.robot.physical_communication(communication)

    def explore_map(self):

        if self.mode_of_exploration == None:
            print("Physical/Virtual mode of exploration is not inited. ")
            exit(0)

        current_node = self.start
        if self.time_limited_exploration != 0:
            start_time = time.time()

        while True:
            # Exploration Completed
            if self.total_explored == 300:
                print("Exploration Completed")
                break

            # Time Limited Exploration
            if self.time_limited_exploration != 0:
                elapsed_time = time.time() - start_time
                if elapsed_time >= self.time_limited_exploration:
                    print("Time Exploration Exceeded")
                    break

            # Coverage Limited Exploration Ended
            if self.coverage_limited_exploration != 0:
                explored_percentage = (self.total_explored / Exploration.SIZE) * 100
                if explored_percentage >= self.coverage_limited_exploration:
                    print("Coverage limited exploration Exceeded! Current Percentage: " + str(explored_percentage))
                    break

            # For Ending exploration from Bluetooth
            if self.stop_exploration:
                print("Exploration: Ending Exploration")
                return

            # Start of code to begin exploration!
            if not self.sprint_moves:
                self.update_sensors_and_map(current_node)

            if self.coverage_limited_exploration != 0:
                print("Current Coverage: " + str(explored_percentage))
            if self.time_limited_exploration != 0:
                print("Current time: %0.2f " % elapsed_time)
            print("Total Nodes Explored:", self.total_explored, "@", current_node.index)

            # Explore Using Left Wall Hug Strategy
            if self.exploration_strategy == Exploration.ExplorationStrategy.Left_Wall_Hug:
                current_node = self.left_wall_hug_logic(current_node)

                # Left Wall Hug have reached the goal.
                if current_node.index == Exploration.GOAL:
                    print("========PASSED GOAL========")
                    self.explored_goal = True

                # Left Wall Hug reached Goal and back at start! Completed Left Wall Hug, Change Strategy!
                if self.explored_goal and current_node.index == self.start.index:
                    if self.sprint_moves:
                        print("========FINISHING LAST SPRINT========")
                        self.sprint_align_only(self.sprint_moves)
                    self.exploration_strategy = self.exploration_strategy.Explore_Middle

            # Explore using AStar to find all unknown grid and travel over!
            elif self.exploration_strategy == Exploration.ExplorationStrategy.Explore_Middle:
                print("========Explore Middle Strategy!!========")
                self.update_sensors_and_map(current_node)
                current_node = self.exploration_sweep(current_node)
                break

        # Return to start after Exploration is completed!
        if current_node != self.start:
            astar = AStar(current_node.index, self.start.index, self.current_map,
                          direction=self.robot.orientation.value)
            path_to_start = astar.main()
            self.sprint_align_only(path_to_start)
            current_node = self.start
            self.robot.orientation = astar.final_orientation
            self.update_sensors_and_map(current_node)

            # arduino_commands = astar.to_arduino_command(path_to_start)
            # print(arduino_commands)
            # self.robot.robot_move_and_wait(arduino_commands)

        # Face Virtual North
        if self.robot.orientation == Robot.Orientation.East:
            self.robot.turn_left()
        elif self.robot.orientation == Robot.Orientation.South:
            self.robot.turn_180()
        elif self.robot.orientation == Robot.Orientation.West:
            self.robot.turn_right()

        self.update_sensors_and_map(current_node)
        print("EXPLORATION FINISHED")

        mdf1 = "MDF1 -> " + mapHandler.generate_mfd1(self.current_map)
        mdf2 = "MDF2 -> " + mapHandler.generate_mfd2(self.current_map)

        # Physical run update android mdf1 and string
        msg = "an#status explored"
        if self.mode_of_exploration == Exploration.ModeOfExploration.Physical_Run:
            time.sleep(0.2)
            self.communication_module.processMsg(msg)
            time.sleep(0.2)
            msg = "an#status:"
            self.communication_module.processMsg(msg + mdf1)
            time.sleep(0.2)
            self.communication_module.processMsg(msg + mdf2)

        print(msg + mdf1)
        print(msg + mdf2)
        return self.current_map

    def exploration_sweep(self, current_node):

        ## Explore remaining unexplored nodes
        while True:
            shortest_path = None
            min_cost = 9999
            i = 1
            # print(self.current_map)
            prev_map = self.current_map[:]

            while True:
                astar = AStar(current_node.index, 0, self.current_map, direction=self.robot.orientation.value)
                neighbours = self.get_floodfield_neighbour(i, current_node.index)
                # print("Neighbours",neighbours,"of node",current_node.index,"of min cost",i)

                for n in neighbours:
                    if self.current_map[n] == '2':
                        astar = AStar(current_node.index, 0, self.current_map, direction=self.robot.orientation.value)
                        surrounding = astar.find_unexplored_neighbours(n,2)
                        # print("Node to explore:", n)
                        # print("Surrounding:",n,"is", surrounding)
                        for s in surrounding:
                            if not self.nodes[s].visited:
                                astar.goal = s
                                astar.generate_nodes()
                                # print("Finding shortest path from",current_node.index,"to node",n,"via",s)
                                path = astar.main()
                                if path != None and astar.total_cost < min_cost:
                                    shortest_path = path
                                    min_cost = astar.total_cost
                                    # print("New shortest path found to node ",n,"via",s)
                                    # print("Min cost",min_cost)

                if shortest_path and min_cost + AStar.TURN_COST <= i:
                    break
                if i < Exploration.HEIGHT + Exploration.WIDTH - 6 + AStar.TURN_COST:
                    i += 1
                else:
                    break
            print()

            if shortest_path:
                # print("Shortest path selected:",shortest_path)
                for move in shortest_path:
                    if move == Robot.ArduinoCommand.Rotate_Left.value:
                        self.robot.turn_left()
                    elif move == Robot.ArduinoCommand.Rotate_Right.value:
                        self.robot.turn_right()
                    elif move == Robot.ArduinoCommand.Rotate_180.value:
                        self.robot.turn_180()
                    elif move == Robot.ArduinoCommand.Forward.value:
                        self.robot.move_forward()
                        if self.robot.orientation == Robot.Orientation.North:
                            current_node = self.nodes[current_node.index + Exploration.WIDTH]
                        elif self.robot.orientation == Robot.Orientation.East:
                            current_node = self.nodes[current_node.index + 1]
                        elif self.robot.orientation == Robot.Orientation.South:
                            current_node = self.nodes[current_node.index - Exploration.WIDTH]
                        elif self.robot.orientation == Robot.Orientation.West:
                            current_node = self.nodes[current_node.index - 1]
                    # self.visit(current_node)
                    print("Now @ X:%d, Y:%d" % (current_node.x, current_node.y))
                    self.update_sensors_and_map(current_node)

                    if prev_map != self.current_map:
                        ## Map has been updated, should calculate new path
                        break
            else:
                break
        return current_node

    def get_floodfield_neighbour(self, total_mvmt, index):
        neighbours = []
        node_x = index % Exploration.WIDTH
        node_y = index // Exploration.WIDTH
        diff = total_mvmt - AStar.TURN_COST

        if diff > 0:
            x = 1
            y = diff
            # print("diff", diff)
            while x <= diff:
                # print("x, y", x, y)
                if node_x < Exploration.WIDTH - x:
                    if node_y < Exploration.HEIGHT - y:
                        neighbour_x = node_x + x
                        neighbour_y = node_y + y
                        neighbours.append(neighbour_y * Exploration.WIDTH + neighbour_x)
                    if node_y > y - 1:
                        neighbour_x = node_x + x
                        neighbour_y = node_y - y
                        neighbours.append(neighbour_y * Exploration.WIDTH + neighbour_x)
                if node_x > x - 1:
                    if node_y < Exploration.HEIGHT - y:
                        neighbour_x = node_x - x
                        neighbour_y = node_y + y
                        neighbours.append(neighbour_y * Exploration.WIDTH + neighbour_x)
                    if node_y > y - 1:
                        neighbour_x = node_x - x
                        neighbour_y = node_y - y
                        neighbours.append(neighbour_y * Exploration.WIDTH + neighbour_x)
                y -= 1
                x += 1
            if node_x > total_mvmt - 1:
                neighbours.append(index - total_mvmt)
            if node_x < Exploration.WIDTH - total_mvmt:
                neighbours.append(index + total_mvmt)
            if node_y > total_mvmt - 1:
                neighbours.append(index - total_mvmt * Exploration.WIDTH)
            if node_y < Exploration.HEIGHT - total_mvmt:
                neighbours.append(index + total_mvmt * Exploration.WIDTH)

        return neighbours

    def left_wall_hug_logic(self, current_node):

        next_move = current_node
        left_move_possible = self.check_direction(current_node, "Left")
        forward_move_possible = self.check_direction(current_node, "Forward")
        right_move_possible = self.check_direction(current_node, "Right")

        if self.sprint_moves and next_move.index == 283:
            self.goal_in_sprint = True

        # Check Move Left
        if left_move_possible and not self.robot.previous_step_left_wall:

            # append move to sprint_moves if next move is already explored
            if self.check_is_fully_explored(next_move.index):
                if not self.sprint_moves:
                    self.sprint_start = current_node
                    self.sprint_orientation = self.robot.orientation
                self.sprint_moves.append(Robot.ArduinoCommand.Rotate_Left.value)
                self.robot.orientation = Robot.Orientation((self.robot.orientation.value - 1) % 4)
                # print(self.sprint_moves)

            # if next move contained unexplored blocks, commence sprint for previous stored moves
            elif self.sprint_moves:
                self.sprint_moves.append(Robot.ArduinoCommand.Rotate_Left.value)
                self.sprint_end = next_move
                next_move = self.sprint_dead_end()

            else:
                self.robot.turn_left()

        # Check Move front
        elif forward_move_possible:

            if self.robot.orientation == Robot.Orientation.North:
                next_move = self.nodes[current_node.index + Exploration.WIDTH]
            elif self.robot.orientation == Robot.Orientation.East:
                next_move = self.nodes[current_node.index + 1]
            elif self.robot.orientation == Robot.Orientation.South:
                next_move = self.nodes[current_node.index - Exploration.WIDTH]
            elif self.robot.orientation == Robot.Orientation.West:
                next_move = self.nodes[current_node.index - 1]

            # append move to sprint_moves if next move is already explored
            if self.check_is_fully_explored(next_move.index):
                if not self.sprint_moves:
                    self.sprint_start = current_node
                    self.sprint_orientation = self.robot.orientation
                self.sprint_moves.append(Robot.ArduinoCommand.Forward.value)
                # print(self.sprint_moves)

            # if next move contained unexplored blocks, commence sprint for previous stored moves
            elif self.sprint_moves:
                self.sprint_moves.append(Robot.ArduinoCommand.Forward.value)
                self.sprint_end = next_move
                next_move = self.sprint_dead_end()

            else:
                self.robot.move_forward()

        # Check Move right
        elif right_move_possible:

            # append move to sprint_moves if next move is already explored
            if self.check_is_fully_explored(next_move.index):
                if not self.sprint_moves:
                    self.sprint_start = current_node
                    self.sprint_orientation = self.robot.orientation
                self.sprint_moves.append(Robot.ArduinoCommand.Rotate_Right.value)
                self.robot.orientation = Robot.Orientation((self.robot.orientation.value + 1) % 4)
                # print(self.sprint_moves)

            # if next move contained unexplored blocks, commence sprint for previous stored moves
            elif self.sprint_moves:
                self.sprint_moves.append(Robot.ArduinoCommand.Rotate_Right.value)
                self.sprint_end = next_move
                next_move = self.sprint_dead_end()

            else:
                self.robot.turn_right()

        # Turn 180
        else:
            if self.check_is_fully_explored(next_move.index):
                if not self.sprint_moves:
                    self.sprint_start = current_node
                    self.sprint_orientation = self.robot.orientation
                self.sprint_moves.append(Robot.ArduinoCommand.Rotate_180.value)
                self.robot.orientation = Robot.Orientation((self.robot.orientation.value - 2) % 4)
                # print(self.sprint_moves)

            # if next move contained unexplored blocks, commence sprint for previous stored moves
            elif self.sprint_moves:
                self.sprint_moves.append(Robot.ArduinoCommand.Rotate_180.value)
                self.sprint_end = next_move
                next_move = self.sprint_dead_end()

            else:
                self.robot.turn_180()

            # if self.sprint_moves:
            #     next_move = self.sprint()
            # self.robot.turn_180()

        # Prevent the robot from turning left twice due to certain situations.
        self.robot.previous_step_left_wall = left_move_possible

        # When length of sprint is too long; likely stuck in a loop
        if len(self.sprint_moves) > 50:
            cur = self.sprint_start
            self.robot.orientation = self.sprint_orientation

            moves = self.split_sprint(self.sprint_moves)
            prev_map = self.current_map[:]

            for move in moves:
                if move == Robot.ArduinoCommand.Rotate_Left.value:
                    self.robot.turn_left()
                elif move == Robot.ArduinoCommand.Rotate_Right.value:
                    self.robot.turn_right()
                elif move == Robot.ArduinoCommand.Rotate_180.value:
                    self.robot.turn_180()
                else:
                    f = list(move)
                    n_forward = int(f[1])
                    if self.robot.orientation == Robot.Orientation.North:
                        cur = self.nodes[cur.index + Exploration.WIDTH * n_forward]
                    elif self.robot.orientation == Robot.Orientation.East:
                        cur = self.nodes[cur.index + n_forward]
                    elif self.robot.orientation == Robot.Orientation.South:
                        cur = self.nodes[cur.index - Exploration.WIDTH * n_forward]
                    elif self.robot.orientation == Robot.Orientation.West:
                        cur = self.nodes[cur.index - n_forward]

                    arduino_move = [Robot.ArduinoCommand.AR.value, "q"]
                    arduino_move.append(move)
                    arduino_move.append(Robot.ArduinoCommand.End.value)
                    print(''.join(arduino_move))
                    self.robot.robot_move_and_wait(''.join(arduino_move))

                self.update_sensors_and_map(cur)
                if self.current_map != prev_map:
                    break

            self.sprint_moves = []
            next_move = cur

        return next_move

    def sprint(self):
        moves = self.sprint_moves
        current_node = self.sprint_start
        self.robot.orientation = self.sprint_orientation
        path = [current_node.index]
        orientations = [self.robot.orientation.value]
        goal_in_sprint = False

        print("SPRINTING FROM", current_node.index)
        for move in moves:
            if move == Robot.ArduinoCommand.Rotate_Left.value:
                self.robot.orientation = Robot.Orientation((self.robot.orientation.value - 1) % 4)
                path.append(-1)
            elif move == Robot.ArduinoCommand.Rotate_Right.value:
                self.robot.orientation = Robot.Orientation((self.robot.orientation.value + 1) % 4)
                path.append(-1)
            elif move == Robot.ArduinoCommand.Rotate_180.value:
                self.robot.orientation = Robot.Orientation((self.robot.orientation.value + 2) % 4)
                path.append(-1)
            elif move == Robot.ArduinoCommand.Forward.value:
                if self.robot.orientation == Robot.Orientation.North:
                    current_node = self.nodes[current_node.index + Exploration.WIDTH]
                elif self.robot.orientation == Robot.Orientation.East:
                    current_node = self.nodes[current_node.index + 1]
                elif self.robot.orientation == Robot.Orientation.South:
                    current_node = self.nodes[current_node.index - Exploration.WIDTH]
                elif self.robot.orientation == Robot.Orientation.West:
                    current_node = self.nodes[current_node.index - 1]
                path.append(current_node.index)
            orientations.append(self.robot.orientation.value)
            if current_node.index == 283:
                goal_in_sprint = True
            # self.gui.paint_arena_explore(self.current_map, current_node.index, self.robot.orientation.value)

        print("goal_in_sprint:",self.goal_in_sprint)
        self.goal_in_sprint = False
        print("sprint_goal_in_sprint:",goal_in_sprint)
        # print("sprint start & end",self.sprint_start.index, current_node.index)
        if goal_in_sprint and self.sprint_start.index != 283 and current_node.index != 283:
            astar = AStar(self.sprint_start.index, current_node.index, self.current_map, waypoint = 283, direction = self.sprint_orientation.value)
            arduino_commands = astar.to_arduino_command(astar.main())
        else:
            # print("no goal in sprint")
            astar = AStar(self.sprint_start.index, current_node.index, self.current_map, direction = self.sprint_orientation.value)
            arduino_commands = astar.to_arduino_command(astar.main())

        print(arduino_commands)
        self.robot.robot_move_and_wait(arduino_commands)

        self.sprint_moves = []
        self.update_sensors_and_map(current_node)
        print("SPRINTED TO", current_node.index)
        print("final orientation according to sprint", self.robot.orientation)

        return current_node

    def sprint_align_only(self, moves):
        path = self.split_sprint(moves)
        for move in path:
            command = [Robot.ArduinoCommand.AR.value, "q"]
            command.append(move)
            command.extend(['a', Robot.ArduinoCommand.End.value])
            print(''.join(command))
            self.robot.robot_move_and_wait(''.join(command))

        self.sprint_moves = []

    def sprint_dead_end(self):
        print("===SRINTING FROM===",self.sprint_start.index)
        current_node = self.sprint_start
        self.robot.orientation = self.sprint_orientation

        if self.goal_in_sprint and self.sprint_start.index != 283 and self.sprint_end.index != 283:
            astar = AStar(self.sprint_start.index, self.sprint_end.index, self.current_map, waypoint=283,
                          direction=self.sprint_orientation.value)
            new_path = astar.main()
        else:
            # print("no goal in sprint")
            astar = AStar(self.sprint_start.index, self.sprint_end.index, self.current_map,
                          direction=self.sprint_orientation.value)
            new_path = astar.main()
        self.goal_in_sprint = False

        print("Original sprint moves:", self.sprint_moves)
        print("Shortened path:", new_path)
        moves = self.split_sprint(new_path)
        prev_map = self.current_map[:]

        for move in moves:
            if move == Robot.ArduinoCommand.Rotate_Left.value:
                self.robot.turn_left()
            elif move == Robot.ArduinoCommand.Rotate_Right.value:
                self.robot.turn_right()
            elif move == Robot.ArduinoCommand.Rotate_180.value:
                self.robot.turn_180()
            else:
                f = list(move)
                n_forward = int(f[1])
                if self.robot.orientation == Robot.Orientation.North:
                    current_node = self.nodes[current_node.index + Exploration.WIDTH * n_forward]
                elif self.robot.orientation == Robot.Orientation.East:
                    current_node = self.nodes[current_node.index + n_forward]
                elif self.robot.orientation == Robot.Orientation.South:
                    current_node = self.nodes[current_node.index - Exploration.WIDTH * n_forward]
                elif self.robot.orientation == Robot.Orientation.West:
                    current_node = self.nodes[current_node.index - n_forward]

                arduino_move = [Robot.ArduinoCommand.AR.value, "q"]
                arduino_move.append(move)
                arduino_move.append(Robot.ArduinoCommand.End.value)
                print(''.join(arduino_move))
                self.robot.robot_move_and_wait(''.join(arduino_move))

            self.update_sensors_and_map(current_node)
            if self.current_map != prev_map:
                break

        self.sprint_moves = []
        print("===SRINTED TO===", current_node.index)
        print("sprint_end:",self.sprint_end.index)

        return current_node

    def split_sprint(self, moves, max_forward=1):
        split_moves = []
        forward_count = 0

        for move in moves:
            if move != Robot.ArduinoCommand.Forward.value:
                if forward_count > 0:
                    f = 'F' + str(forward_count)
                    split_moves.append(f)
                    forward_count = 0
                split_moves.append(move)
            else:
                if forward_count < max_forward:
                    forward_count += 1
                else:
                    f = 'F' + str(max_forward)
                    split_moves.append(f)
                    forward_count = 1

        if forward_count > 0:
            f = 'F' + str(forward_count)
            split_moves.append(f)

        print("Split sprint moves:", split_moves)
        return split_moves

    def get_virtual_senor_update(self, current_node):

        if self.robot.orientation == Robot.Orientation.North:
            # Forward of Virtual North
            pos = current_node.index + Exploration.WIDTH * 2
            front_left = [pos - 1, pos + 14]
            front_mid = [pos, pos + 15]
            front_right = [pos + 1, pos + 16]
            for i in front_left:
                self.update_virtual_map(i)
                if i <= 299 and self.current_map[i] == self.mh.is_obstacle:
                    break
            for i in front_mid:
                self.update_virtual_map(i)
                if i <= 299 and self.current_map[i] == self.mh.is_obstacle:
                    break
            for i in front_right:
                self.update_virtual_map(i)
                if i <= 299 and self.current_map[i] == self.mh.is_obstacle:
                    break

            # Left of Virtual North
            pos = current_node.index - 2
            left_top = [pos + 15, pos + 14]
            left_bottom = [pos - 15, pos - 16]
            for i in left_top:
                #
                if (i // Exploration.WIDTH) == current_node.y + 1:
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

            for i in left_bottom:
                if (i // Exploration.WIDTH) == current_node.y - 1:
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

            # Right of Virtual North
            pos = current_node.index + 2
            right_top = [pos + 15, pos + 16]
            for i in right_top:
                if (i // 15) == current_node.y + 1:
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

        elif self.robot.orientation == Robot.Orientation.East:

            # Forward of Virtual East
            pos = current_node.index + 2
            front_top = [pos + 15, pos + 16]
            front_mid = [pos, pos + 1]
            front_btm = [pos - 15, pos - 14]
            for i in front_top:
                if (i // Exploration.WIDTH) == (current_node.y + 1):
                    self.update_virtual_map(i)
                    if i <= 299 and self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in front_mid:
                if (i // Exploration.WIDTH) == current_node.y:
                    self.update_virtual_map(i)
                    if i <= 299 and self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in front_btm:
                if (i // Exploration.WIDTH) == (current_node.y - 1):
                    self.update_virtual_map(i)
                    if i <= 299 and self.current_map[i] == self.mh.is_obstacle:
                        break

            # Left of Virtual East
            pos = current_node.index + Exploration.WIDTH * 2
            left_top = [pos + 1, pos + 16]
            left_btm = [pos - 1, pos + 14]
            for i in left_top:
                if i <= 299 and (i % 15) == (current_node.x + 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in left_btm:
                if i <= 299 and (i % 15) == (current_node.x - 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

            # Right of Virtual East
            pos = current_node.index - Exploration.WIDTH * 2
            right_top = [pos + 1, pos - 14]
            for i in right_top:
                if (i >= 0) and (i % 15) == (current_node.x + 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

        elif self.robot.orientation == Robot.Orientation.South:

            # Forward of Virtual South
            pos = current_node.index - Exploration.WIDTH * 2
            front_left = [pos + 1, pos - 14]
            front_mid = [pos, pos - 15]
            front_right = [pos - 1, pos - 16]

            for i in front_left:
                self.update_virtual_map(i)
                if i >= 0 and self.current_map[i] == self.mh.is_obstacle:
                    break
            for i in front_mid:
                self.update_virtual_map(i)
                if i >= 0 and self.current_map[i] == self.mh.is_obstacle:
                    break
            for i in front_right:
                self.update_virtual_map(i)
                if i >= 0 and self.current_map[i] == self.mh.is_obstacle:
                    break

            # Left of Virtual South
            pos = current_node.index + 2
            left_top = [pos - 15, pos - 14]
            left_btm = [pos + 15, pos + 16]
            for i in left_top:
                if (i // Exploration.WIDTH) == (current_node.y - 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in left_btm:
                if (i // Exploration.WIDTH) == (current_node.y + 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

            # Right of Virtual South
            pos = current_node.index - 2
            right_top = [pos - 15, pos - 16]
            for i in right_top:
                if (i // Exploration.WIDTH) == (current_node.y - 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

        elif self.robot.orientation == Robot.Orientation.West:
            pos = current_node.index - 2
            front_left = [pos - 15, pos - 16]
            front_mid = [pos, pos - 1]
            front_right = [pos + 15, pos + 14]

            for i in front_left:
                if (i // Exploration.WIDTH) == (current_node.y - 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in front_mid:
                if (i // Exploration.WIDTH) == current_node.y:
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in front_right:
                if (i // Exploration.WIDTH) == (current_node.y + 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

            # Left of West
            pos = current_node.index - Exploration.WIDTH * 2
            left_top = [pos - 1, pos - 16]
            left_btm = [pos + 1, pos - 14]

            for i in left_top:
                if (i > 0) and (i % Exploration.WIDTH) == (current_node.x - 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break
            for i in left_btm:
                if (i > 0) and (i % Exploration.WIDTH) == (current_node.x + 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

            # Right of West
            pos = current_node.index + Exploration.WIDTH * 2
            right_top = [pos - 1, pos + 14]
            for i in right_top:
                if (i < 300) and (i % Exploration.WIDTH) == (current_node.x - 1):
                    self.update_virtual_map(i)
                    if self.current_map[i] == self.mh.is_obstacle:
                        break

    def update_virtual_map(self, location):
        if location > 299 or location < 0:
            return

        if self.current_map[location] == "2":
            self.total_explored += 1

        self.current_map[location] = self.obstacle_map[location]

    def check_is_fully_explored(self, index, n_spaces = 2):
        # print("checking for index", index)
        left = [index - 2, index - 17, index + 13]
        right = [index + 2, index + 17, index - 13]
        top = [index + 30, index + 31, index + 29]
        btm = [index - 30, index - 31, index - 29]

        for n in range(1, n_spaces+1):
            if (index % Exploration.WIDTH) - n > 0:
                # print("checking left",n)
                left_copy = []
                for i in left:
                    if self.current_map[i] == '2':
                        return False
                    elif self.current_map[i] == '0':
                        left_copy.append(i - 1)
                left = left_copy
            if (index % Exploration.WIDTH) + n < Exploration.WIDTH - 1:
                # print("checking right", n)
                right_copy = []
                for i in right:
                    if self.current_map[i] == '2':
                        return False
                    elif self.current_map[i] == '0':
                        right_copy.append(i + 1)
                right = right_copy
            if (index // Exploration.WIDTH) - n > 0:
                # print("checking btm", n)
                btm_copy = []
                for i in btm:
                    if self.current_map[i] == '2':
                        return False
                    elif self.current_map[i] == '0':
                        btm_copy.append(i - 15)
                btm = btm_copy
            if (index // Exploration.WIDTH) + n < Exploration.HEIGHT - 1:
                # print("checking top", n)
                top_copy = []
                for i in top:
                    if self.current_map[i] == '2':
                        return False
                    elif self.current_map[i] == '0':
                        top_copy.append(i + 15)
                top = top_copy

        # print(index,"is fully explored\n")
        return True

    def check_direction(self, current_node, direction):

        # Get the relation direction of the robot before checking
        relative_direction = None
        if direction == "Left":
            relative_direction = (self.robot.orientation.value - 1) % 4
        elif direction == "Right":
            relative_direction = (self.robot.orientation.value + 1) % 4
        elif direction == "Forward":
            relative_direction = self.robot.orientation.value

        if relative_direction == Robot.Orientation.North.value:
            return self.check_virtual_north(current_node)

        elif relative_direction == Robot.Orientation.East.value:
            return self.check_virtual_east(current_node)

        elif relative_direction == Robot.Orientation.South.value:
            return self.check_virtual_south(current_node)

        elif relative_direction == Robot.Orientation.West.value:
            return self.check_virtual_west(current_node)

    def check_virtual_north(self, current_node):
        # Check forward possible
        if current_node.y >= 18:
            # print("Virtual North not possible")
            return False

        for i in self.mh.check_front_grid:
            # Check if forward have obstacle
            if self.current_map[current_node.index + i] == self.mh.is_obstacle:
                return False
        return True

    def check_virtual_south(self, current_node):
        for i in self.mh.check_bottom_grid:
            # Check possible
            if current_node.y <= 1:
                return False
            # Check obstacle
            elif self.current_map[current_node.index + i] == self.mh.is_obstacle:
                return False

        return True

    def check_virtual_east(self, current_node):
        # Check for out of bounds
        # print("current node: X: %d, Y: %d" % (current_node.x, current_node.y))
        if current_node.x >= 13:
            return False

        for i in self.mh.check_right_grid:
            # print("Checking right grid: x:%d,y:%d" % (current_node.x, current_node.y))
            # print("Index: %d" % (current_node.index))
            if self.current_map[current_node.index + i] == self.mh.is_obstacle:
                return False
        return True

    def check_virtual_west(self, current_node):

        for i in self.mh.check_virtual_west:
            # print("Checking left grid: x:%d,y:%d" % (current_node.x + i, current_node.y + i))
            # Check left move possible
            if current_node.x <= 1:
                # print("Left move not possible")
                return False
            # Check if left side has any obstacle
            elif self.current_map[current_node.index + i] == self.mh.is_obstacle:
                # print("Left has obstacle")
                return False

        return True

    def update_sensors_and_map(self, current_node):

        # Update for virtual Run
        if self.mode_of_exploration == Exploration.ModeOfExploration.Virtual_Run:
            self.get_virtual_senor_update(current_node)
            self.gui.paint_arena_explore(self.current_map, current_node.index, self.robot.orientation.value)
            time.sleep(self.steps_per_second)

        # Update for Physical Run
        elif self.mode_of_exploration == Exploration.ModeOfExploration.Physical_Run:

            # Get updated map from physical sensors
            self.current_map = self.robot.get_sensor_data(current_map = self.current_map,
                                                          current_index= current_node.index)

            # Preparation for updating android map.
            map = self.current_map[:]
            dir = str(self.robot.orientation.value)
            andr = "an#"
            mode = "obs:"
            robot = [current_node.index - 1, current_node.index + 1,  current_node.index,
                     current_node.index + Exploration.WIDTH, current_node.index + Exploration.WIDTH + 1,
                     current_node.index + Exploration.WIDTH - 1,
                     current_node.index - Exploration.WIDTH, current_node.index - Exploration.WIDTH + 1,
                     current_node.index - Exploration.WIDTH - 1]

            #Put robot in map
            for i in robot:
                map[i] = "3"

            map_buf = ""
            for i in map:
                map_buf = map_buf + str(i) + ","

            android_buffer = andr + dir + mode + map_buf
            # Send to android
            print(android_buffer)
            self.communication_module.processMsg(android_buffer)

            # Send to PC Simulator
            pc_buffer = "wifi#pcsim:" + ''.join(self.current_map) + "," + str(current_node.index) + "," + str(self.robot.orientation.value)
            self.communication_module.processMsg(pc_buffer)
