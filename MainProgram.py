from Exploration import Exploration
from mainUI import *
import time
import threading
import astar
import robot
import multiprocessing


class main_program:

    UI = None
    Explore = None
    robot = None
    e = None
    exploration_thread = None

    def __init__(self):
        print("Main Program Init")
        self.UI = MainUI()
        self.UI.run()

    def tkinterGui(self):
        self.UI.root.mainloop()

    def exploreThread(self):
        while True:
            if self.UI.ended == True:
                break

            if self.UI.exploration_start == True:
                self.UI.exploration_start = False
                self.e = Exploration(start_position=16)
                self.e.init_virtual_exploration(gui = self.UI,
                                           time_limited_exploration=self.UI.entry_time_limited.get(),
                                           coverage_limited_exploration=self.UI.entry_exploration_target.get(),
                                           steps_per_second=self.UI.entry_step_per_second.get(),
                                           obstacle_map_path=''.join(self.UI.arenaMap))
                # self.exploration_thread = threading.Thread(target=self.e.explore_map)
                # self.exploration_thread.start()
                self.e.explore_map()

            elif self.UI.start_fastest_path == True:
                print(self.UI.entry_step_per_second.get())
                self.UI.start_fastest_path = False
                self.robot = Robot(Robot.Orientation.North)
                current_index = 16
                fastest_path = AStar(current_index, 283, ''.join(self.UI.arenaMap), direction = Robot.Orientation.North.value, waypoint=28).main()
                for move in fastest_path:
                    if move == Robot.ArduinoCommand.Rotate_Left.value:
                        self.robot.turn_left()
                    elif move == Robot.ArduinoCommand.Rotate_Right.value:
                        self.robot.turn_right()
                    elif move == Robot.ArduinoCommand.Rotate_180.value:
                        self.robot.turn_180()
                    elif move == Robot.ArduinoCommand.Forward.value:
                        self.robot.move_forward()
                        if self.robot.orientation == Robot.Orientation.North:
                            current_index = current_index + 15
                        elif self.robot.orientation == Robot.Orientation.East:
                            current_index = current_index + 1
                        elif self.robot.orientation == Robot.Orientation.South:
                            current_index = current_index - 15
                        elif self.robot.orientation == Robot.Orientation.West:
                            current_index = current_index - 1

                    self.UI.paint_arena_explore(self.UI.arenaMap, current_index, self.robot.orientation.value)
                    time.sleep( 1 / int( self.UI.entry_step_per_second.get() ) )


    def main(self):

        explore = threading.Thread(target=self.exploreThread)
        explore.start()
        # explore = multiprocessing.Process(target=self.exploreThread)
        # explore.start()

        self.tkinterGui()

mp = main_program()
mp.main()
