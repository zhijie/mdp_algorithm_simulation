from enum import Enum
import math
from mapHandler import mapHandler

class Robot:

    WIDTH = 15
    HEIGHT = 20
    SIZE = WIDTH * HEIGHT

    OBSTACLE = "1"
    EXPLORED = "0"

    class Orientation(Enum):
        North = 0
        South = 2
        East = 1
        West = 3

    class ArduinoCommand(Enum):
        Rotate_Left = "L"
        Rotate_Right = "R"
        Rotate_180 = "U"
        Forward = "F"
        End = "E"
        Sense = "S"
        AR = "ar#"

    class SensorFlag(Enum):
        B = "BLIND"  # Append a block in front of the specific sensor
        I = "INF"    # Do not append a block at the end
        N = "Normal" # Append a block at the end

    class id(Enum):
        # Priority 9 Highest, 0 Lowest
        S0 = 5
        S1 = 5
        S2 = 5
        S3 = 5
        S4 = 5
        S5 = 5
        robot = 9
        empty = 0

    def __init__(self, orientation=Orientation.North, current_position=50):
        self.orientation = self.Orientation(orientation)
        self.currentPosition = current_position
        self.previous_step_left_wall = False
        self.communication = None
        self.physical_exploration = False
        self.current_map = None

        # Init all map to be empty
        self.current_map_id = []
        for i in range(self.SIZE):
            self.current_map_id.append(self.id.empty.value)

        self.current_index = None
        self.sensor_tag = [self.SensorFlag.N.value,self.SensorFlag.N.value,
                           self.SensorFlag.N.value,self.SensorFlag.N.value,
                           self.SensorFlag.N.value,self.SensorFlag.N.value ]


    def physical_communication(self, communication):
        self.communication = communication
        self.physical_exploration = True

    def get_current_position(self):
        return self.currentPosition

    def set_current_position(self, position):
        self.currentPosition = position


    def rounding_sensor_value(self, sensorValue):
        sensorValue = sensorValue.split(',')
        print("RAW:", sensorValue)
        for i in range(6):
            # Set Sensor flag BLIND
            if self.SensorFlag.B.value in sensorValue[i]:
                self.sensor_tag[i] = self.SensorFlag.B.value
                sensorValue[i] = 0

            # Set Sensor flag INF
            elif self.SensorFlag.I.value in sensorValue[i]:
                self.sensor_tag[i] = self.SensorFlag.I.value
                sensorValue[i] = int(float(sensorValue[i][3:]))

            elif int(float(sensorValue[i])) < 5:
                self.sensor_tag[i] = self.SensorFlag.B.value
                sensorValue[i] = 0

            else:
                self.sensor_tag[i] = self.SensorFlag.N.value
                sensorValue[i] = int(float(sensorValue[i]))

            # Round Values
            if sensorValue[i] % 10 < 5:
                sensorValue[i] = int(math.floor(sensorValue[i] / 10))
            else:
                sensorValue[i] = int(math.ceil(sensorValue[i] / 10))

        print("Con:", sensorValue)
        print("fla", self.sensor_tag)
        return sensorValue


    def get_sensor_value_from_arduino(self):

        if(self.physical_exploration):
            message = Robot.ArduinoCommand.Sense.value + Robot.ArduinoCommand.End.value
            self.communication.processMsg(Robot.ArduinoCommand.AR.value + message)
            message = ""
            while True:
                message = self.communication.get_algo_queue()
                # sensor:50,30,40..
                if "sensor" in message:
                    break
            message = message.split(":")
            message = ''.join(message[1:])
        else:
            # Virtual Simulation
            s0 = "0.3"
            s1 = "0.3"
            s2 = "0.5"
            s3 = "0.3"
            s4 = "0.6"
            s5 = "0.2"

            # s0 = "20"
            # s1 = "20"
            # s2 = "20"
            # s3 = "20"
            # s4 = "20"
            # s5 = "20"
            #

            # s0 = "BLIND"
            # s1 = "BLIND"
            # s2 = "BLIND"
            # s3 = "BLIND"
            # s4 = "BLIND"
            # s5 = "BLIND"

            # s0 = "10"
            # s1 = "20"
            # s2 = "30"
            # s3 = "10"
            # s4 = "20"
            # s5 = "10"



            message = s0 + "," + \
                      s1 + "," + \
                      s2 + "," + \
                      s3 + "," + \
                      s4 + "," + s5

        return message

    def get_sensor_data(self, current_map, current_index):
        self.current_map = current_map
        self.current_index = current_index

        message = self.get_sensor_value_from_arduino()
        sensor_value = self.rounding_sensor_value(message)
        # print("Raw Sensor Values:", sensor_value)
        self.update_map_robot()
        # Value is within the robot so can check axis changed
        east_mid = current_index + 1
        north_mid = current_index + self.WIDTH
        west_mid = current_index - 1
        south_mid = current_index - self.WIDTH

        sensor_start = []
        if self.orientation == Robot.Orientation.North:

            sensor_start.append(north_mid - 1)
            sensor_start.append(north_mid)
            sensor_start.append(north_mid + 1)
            sensor_start.append(west_mid + self.WIDTH)
            sensor_start.append(west_mid)
            sensor_start.append(east_mid + self.WIDTH)
            self.north_sensor_update(sensor_value[0], sensor_start[0], self.id.S0, self.sensor_tag[0])
            self.north_sensor_update(sensor_value[1], sensor_start[1], self.id.S1, self.sensor_tag[1])
            self.north_sensor_update(sensor_value[2], sensor_start[2], self.id.S2, self.sensor_tag[2])
            self.west_sensor_update(sensor_value[3], sensor_start[3], self.id.S3, self.sensor_tag[3])
            self.west_sensor_update(sensor_value[4], sensor_start[4], self.id.S4, self.sensor_tag[4])
            self.east_sensor_update(sensor_value[5], sensor_start[5], self.id.S5, self.sensor_tag[5])

            #print(sensor_value)
            #print(sensor_start)
            #print(self.sensor_tag)
            #mapHandler.print_flipped_grid(self.current_map)

        elif self.orientation == Robot.Orientation.East:
            sensor_start.append(east_mid + self.WIDTH)
            sensor_start.append(east_mid)
            sensor_start.append(east_mid - self.WIDTH)
            sensor_start.append(north_mid + 1)
            sensor_start.append(north_mid)
            sensor_start.append(south_mid + 1)

            self.east_sensor_update(sensor_value[0], sensor_start[0], self.id.S0, self.sensor_tag[0])
            self.east_sensor_update(sensor_value[1], sensor_start[1], self.id.S1, self.sensor_tag[1])
            self.east_sensor_update(sensor_value[2], sensor_start[2], self.id.S2, self.sensor_tag[2])
            self.north_sensor_update(sensor_value[3], sensor_start[3], self.id.S3, self.sensor_tag[3])
            self.north_sensor_update(sensor_value[4], sensor_start[4], self.id.S4, self.sensor_tag[4])
            self.south_sensor_update(sensor_value[5], sensor_start[5], self.id.S5, self.sensor_tag[5])

        elif self.orientation == Robot.Orientation.South:
            sensor_start.append(south_mid + 1)
            sensor_start.append(south_mid)
            sensor_start.append(south_mid - 1)
            sensor_start.append(east_mid - self.WIDTH)
            sensor_start.append(east_mid)
            sensor_start.append(west_mid - self.WIDTH)

            self.south_sensor_update(sensor_value[0], sensor_start[0], self.id.S0, self.sensor_tag[0])
            self.south_sensor_update(sensor_value[1], sensor_start[1], self.id.S1, self.sensor_tag[1])
            self.south_sensor_update(sensor_value[2], sensor_start[2], self.id.S2, self.sensor_tag[2])
            self.east_sensor_update(sensor_value[3], sensor_start[3], self.id.S3, self.sensor_tag[3])
            self.east_sensor_update(sensor_value[4], sensor_start[4], self.id.S4, self.sensor_tag[4])
            self.west_sensor_update(sensor_value[5], sensor_start[5], self.id.S5, self.sensor_tag[5])

        elif self.orientation == Robot.Orientation.West:
            sensor_start.append(west_mid - self.WIDTH)
            sensor_start.append(west_mid)
            sensor_start.append(west_mid + self.WIDTH)
            sensor_start.append(south_mid - 1)
            sensor_start.append(south_mid)
            sensor_start.append(north_mid - 1)

            self.west_sensor_update(sensor_value[0], sensor_start[0], self.id.S0, self.sensor_tag[0])
            self.west_sensor_update(sensor_value[1], sensor_start[1], self.id.S1, self.sensor_tag[1])
            self.west_sensor_update(sensor_value[2], sensor_start[2], self.id.S2, self.sensor_tag[2])
            self.south_sensor_update(sensor_value[3], sensor_start[3], self.id.S3, self.sensor_tag[3])
            self.south_sensor_update(sensor_value[4], sensor_start[4], self.id.S4, self.sensor_tag[4])
            self.north_sensor_update(sensor_value[5], sensor_start[5], self.id.S5, self.sensor_tag[5])
        #
        return self.current_map

    def west_sensor_update(self, value, start, id, type):
        y_axis = int(start/self.WIDTH)
        current_location = start

        for i in range (value):
            current_location = current_location - 1
            if int(current_location/self.WIDTH) != y_axis:
                return
            self.update_map(current_location, id, self.EXPLORED)

        # Process for BLIND/NORMAL, INF don't need do anything
        current_location = current_location - 1
        if self.SensorFlag.B.value in type and y_axis == int(current_location/self.WIDTH):
                self.update_map(current_location, id, self.OBSTACLE)

        elif self.SensorFlag.N.value in type and y_axis == int(current_location/self.WIDTH):
                self.update_map(current_location, id, self.OBSTACLE)

    '''
        Args:
            value: Number of blocks to update
            start: The start of the block to update
            id: The object that updates map
            type: INF/BLIND/NORMAL
    '''
    def north_sensor_update(self, value, start, id , type):

        current_location = start

        for i in range(value):
            current_location = current_location + self.WIDTH # inc northwards
            if current_location > 299: # exceed map break
                return
            self.update_map(current_location, id, self.EXPLORED) # update map with explored

        # Process BLIND/NORMAL
        current_location = current_location + self.WIDTH
        if current_location > 299:
            return

        if self.SensorFlag.B.value in type:
            self.update_map(current_location, id, self.OBSTACLE)
        elif self.SensorFlag.N.value in type:
            self.update_map(current_location, id, self.OBSTACLE)

    def east_sensor_update(self, value, start, id , type):
        y_axis = int(start/self.WIDTH)
        current_location = start

        for i in range (value):
            current_location = current_location + 1
            if int(current_location/self.WIDTH) != y_axis:
                return
            self.update_map(current_location, id, self.EXPLORED)

        # Process for BLIND/NORMAL, INF don't need do anything
        current_location = current_location + 1
        if self.SensorFlag.B.value in type and y_axis == int(current_location/self.WIDTH):
                self.update_map(current_location, id, self.OBSTACLE)

        elif self.SensorFlag.N.value in type and y_axis == int(current_location/self.WIDTH):
                self.update_map(current_location, id, self.OBSTACLE)

    def south_sensor_update(self, value, start, id , type):
        current_location = start

        for i in range(value):
            current_location = current_location - self.WIDTH
            if current_location < 0:
                return
            self.update_map(current_location, id, self.EXPLORED)

        # Process BLIND/NORMAL
        current_location = current_location - self.WIDTH
        if current_location < 0:
            return

        if self.SensorFlag.B.value in type:
            self.update_map(current_location, id, self.OBSTACLE)
        elif self.SensorFlag.N.value in type:
            self.update_map(current_location, id, self.OBSTACLE)

    def update_map(self, location, id, obstacle):
        if location >= 0 and location <= 299 and \
           self.current_map_id[location] <= id.value:
            if obstacle == self.EXPLORED:
                self.current_map[location] = "0"
                self.current_map_id[location] = id.value
            elif obstacle == self.OBSTACLE:
                self.current_map[location] = "1"
                self.current_map_id[location] = id.value

    def update_map_robot(self):
        robot = [
            self.current_index + 14, self.current_index + 15,   self.current_index + 16,
            self.current_index - 1,  self.current_index,        self.current_index + 1,
            self.current_index - 16, self.current_index - 15,   self.current_index - 14
        ]

        for i in robot:
            self.update_map(i, self.id.robot, self.EXPLORED)

    def turn_left(self):
        self.orientation = self.Orientation((self.orientation.value - 1) % 4)
        if self.physical_exploration:

            msg = Robot.ArduinoCommand.AR.value + Robot.ArduinoCommand.Rotate_Left.value + Robot.ArduinoCommand.AR.End.value
            self.robot_move_and_wait(msg)

    def turn_right(self):
        self.orientation = self.Orientation((self.orientation.value + 1) % 4)
        if self.physical_exploration:
            msg = Robot.ArduinoCommand.AR.value + Robot.ArduinoCommand.Rotate_Right.value + Robot.ArduinoCommand.AR.End.value
            self.robot_move_and_wait(msg)

    def move_forward(self):

        if self.physical_exploration:
            msg = Robot.ArduinoCommand.AR.value + Robot.ArduinoCommand.Forward.value + "1" + Robot.ArduinoCommand.AR.End.value
            self.robot_move_and_wait(msg)

    def turn_180(self):
        self.orientation = self.Orientation((self.orientation.value + 2) % 4)
        if self.physical_exploration:
            msg = Robot.ArduinoCommand.AR.value + Robot.ArduinoCommand.Rotate_180.value + Robot.ArduinoCommand.AR.End.value
            self.robot_move_and_wait(msg)

    def robot_move_and_wait(self, cmd):

        if self.physical_exploration:
            self.communication.processMsg(cmd)
            while True:
                recv_msg = self.communication.get_algo_queue()
                if "done" in recv_msg:
                    break
                else:
                    print("ALGO MAY BE THROWN AWAY THESE MESSAGES!  --->" + recv_msg)

