import math
from enum import Enum
from mapHandler import *
from robot import *


class AStar:
    WIDTH = 15
    HEIGHT = 20
    SIZE = WIDTH * HEIGHT
    MVMT_COST = 1
    TURN_COST = 3

    class Orientation(Enum):
        NORTH = 0
        SOUTH = 2
        EAST = 1
        WEST = 3

    class Node:
        def __init__(self, index):
            self.index = index
            self.x = index % AStar.WIDTH
            self.y = index // AStar.WIDTH
            self.parent = None
            self.valid = True
            self.g = [math.inf, math.inf]
            self.h = [math.inf, math.inf]
            self.f = [math.inf, math.inf]

    def __init__(self, start, goal, map, waypoint=0, direction=Orientation.NORTH.value):
        self.start = start
        self.waypoint = waypoint
        self.goal = goal
        self.dir = direction
        self.final_orientation = None
        self.map = map
        self.nodes = []
        self.generate_nodes()
        self.shortest_path = []
        self.directions = []
        self.n_turns = 0
        self.total_cost = 0

    def main(self):
        if not (self.check_node(self.start)) or not (self.check_node(self.goal)):
            print("Failed to find path from node", self.start, "to node", self.goal)
            return None
        if not (self.waypoint):
            ## no waypoint specified
            if self.astar_search(self.start, self.goal, 0):
                self.shortest_path = self.generate_shortest_path(self.start, self.goal)
                self.generate_directions()
                if self.start == 16 and self.goal ==283:
                    print("Shortest path:", self.shortest_path)
                    print("List of directions:", self.directions)
                    print("Number of grids traversed:", len(self.shortest_path))
                    print("Number of turns:", self.n_turns, "\n")
                # self.update_sensors_and_map()
                return self.directions
        else:
            ## with waypoint
            if not (self.check_node(self.waypoint)):
                print("Failed to find path from node", self.start, "to waypoint", self.waypoint)
                return None
            if self.astar_search(self.start, self.waypoint, 0):
                path1 = self.generate_shortest_path(self.start, self.waypoint)
                if self.astar_search(self.waypoint, self.goal, 1):
                    path2 = self.generate_shortest_path(self.waypoint, self.goal)
                    self.shortest_path.extend(path1)
                    self.shortest_path.extend(path2[1:])
                    self.generate_directions()
                    print("Shortest path:", self.shortest_path)
                    print("List of directions:", self.directions)
                    print("Number of grids traversed:", len(self.shortest_path))
                    print("Number of turns:", self.n_turns, "\n")
                    self.print_map()
                    return self.directions
        self.print_map()
        print("Failed to find path from node", self.start, "to node", self.goal)
        return None

    def astar_search(self, start, goal, i):
        opened = [start]
        closed = []
        direction = self.dir
        self.nodes[start].g[i] = 0
        while len(opened) != 0:
            # if goal == 231 and start == 199:
            # print("Opened:",opened)
            # print("Closed:",closed)
            cur = opened.pop()
            # if goal == 231 and start == 199:
            # print("Cur:",cur)
            if cur != self.start:
                direction = self.calc_new_dir(self.nodes[cur], self.nodes[cur].parent)
            neighbours = self.find_neighbours(cur)
            # if goal == 231 and start == 199:
            # print("Neighbours:",neighbours)
            # print()
            for n in neighbours:
                if n in closed:
                    continue
                g = self.calc_g_cost(n, cur, i, direction)
                # if goal == 231 and start == 199:
                #     print("n:", n)
                #     print("parent of n:", self.nodes[n].parent.index)
                #     print("g of n:", self.nodes[n].g[i])
                #     print("h of n:", self.nodes[n].h[i])
                #     print("f of n:", self.nodes[n].f[i])
                #     print()
                if g < self.nodes[n].g[i]:
                    self.nodes[n].parent = self.nodes[cur]
                    self.nodes[n].g[i] = g
                    self.nodes[n].h[i] = self.calc_h_cost(n, goal)
                    self.nodes[n].f[i] = self.nodes[n].g[i] + self.nodes[n].h[i]
                # else:
                #     print("========g,g[i]",g,self.nodes[n].g[i])
                # print("checking: n, goal",n,goal)
                if n == goal:
                    # print("Goal found")
                    closed.append(cur)
                    closed.append(n)
                    return True
                else:
                    if n in opened:
                        opened.remove(n)
                    opened = self.add_priority_queue(opened, n, i)
            closed.append(cur)
        print("Unable to find path to goal", goal)
        return False

    def calc_g_cost(self, node, prev, i, direction):
        new_dir = self.calc_new_dir(self.nodes[node], self.nodes[prev])
        if new_dir == direction:
            turn = 0
        # elif (direction + 1) % 4 == new_dir or (direction - 1) % 4 == new_dir:
        #     turn = 1
        else:
            turn = 1
        return self.nodes[prev].g[i] + self.MVMT_COST + turn * self.TURN_COST

    def calc_h_cost(self, n, goal):
        goal_x = goal % AStar.WIDTH
        goal_y = goal // AStar.WIDTH
        return abs(self.nodes[n].x - goal_x) + abs(self.nodes[n].y - goal_y)

    def calc_new_dir(self, node, prev):
        if prev.y != node.y:
            if node.y > prev.y:
                return self.Orientation.NORTH.value
            else:
                return self.Orientation.SOUTH.value
        elif prev.x != node.x:
            if node.x > prev.x:
                return self.Orientation.EAST.value
            else:
                return self.Orientation.WEST.value

    def add_priority_queue(self, opened, n, i):
        ''' Adds the node to the priority queue
            from highest to lowest f. '''
        for j in range(len(opened), 0, -1):
            if self.nodes[n].f[i] <= self.nodes[opened[j - 1]].f[i]:
                opened.insert(j, n)
                return opened
        opened.insert(0, n)
        return opened

    def find_neighbours(self, index):
        ''' Return immediate nearest neighbours (1 grid away)'''
        neighbours = []
        if self.nodes[index].y > 1 and self.nodes[index - AStar.WIDTH].valid:
            neighbours.append(index - AStar.WIDTH)
        if self.nodes[index].y < AStar.HEIGHT - 2 and self.nodes[index + AStar.WIDTH].valid:
            neighbours.append(index + AStar.WIDTH)
        if self.nodes[index].x > 1 and self.nodes[index - 1].valid:
            neighbours.append(index - 1)
        if self.nodes[index].x < AStar.WIDTH - 2 and self.nodes[index + 1].valid:
            neighbours.append(index + 1)
        return neighbours

    def find_neighbours2(self, index):
        ''' Return nearest neighbours (2 grids away)'''
        neighbours = []
        if self.nodes[index].y > 2:
            if self.nodes[index - 2*AStar.WIDTH].valid:
                neighbours.append(index - 2*AStar.WIDTH)
            if self.nodes[index].x > 1 and self.nodes[index - 2*AStar.WIDTH - 1].valid:
                neighbours.append(index - 2*AStar.WIDTH - 1)
            if self.nodes[index].x < AStar.WIDTH - 2 and self.nodes[index - 2*AStar.WIDTH + 1].valid:
                neighbours.append(index - 2*AStar.WIDTH + 1)
        if self.nodes[index].y < AStar.HEIGHT - 3:
            if self.nodes[index + 2*AStar.WIDTH].valid:
                neighbours.append(index + 2*AStar.WIDTH)
            if self.nodes[index].x < AStar.WIDTH - 2 and self.nodes[index + 2*AStar.WIDTH + 1].valid:
                neighbours.append(index + 2*AStar.WIDTH + 1)
            if self.nodes[index].x > 1 and self.nodes[index + 2*AStar.WIDTH - 1].valid:
                neighbours.append(index + 2*AStar.WIDTH - 1)
        if self.nodes[index].x > 2:
            if self.nodes[index - 2].valid:
                neighbours.append(index - 2)
            if self.nodes[index].y < AStar.HEIGHT - 2 and self.nodes[index - 2 + AStar.WIDTH].valid:
                neighbours.append(index - 2 + AStar.WIDTH)
            if self.nodes[index].y > 1 and self.nodes[index - 2 - AStar.WIDTH].valid:
                neighbours.append(index - 2 - AStar.WIDTH)
        if self.nodes[index].x < AStar.WIDTH - 3:
            if self.nodes[index + 2].valid:
                neighbours.append(index + 2)
            if self.nodes[index].y < AStar.HEIGHT - 2 and self.nodes[index + 2 + AStar.WIDTH].valid:
                neighbours.append(index + 2 + AStar.WIDTH)
            if self.nodes[index].y > 1 and self.nodes[index + 2 - AStar.WIDTH].valid:
                neighbours.append(index + 2 - AStar.WIDTH)
        return neighbours

    def find_unexplored_neighbours(self, index, n_grids_away):
        """ n_grids_away is counted from the center of the robot to the unexplored grid """
        n = n_grids_away
        neighbours = []
        if self.nodes[index].y > n:
            if self.nodes[index - n * AStar.WIDTH].valid:
                neighbours.append(index - n * AStar.WIDTH)
            if self.nodes[index].x > 1 and self.nodes[index - n * AStar.WIDTH - 1].valid:
                neighbours.append(index - n * AStar.WIDTH - 1)
            if self.nodes[index].x < AStar.WIDTH - n + 1 and self.nodes[index - n * AStar.WIDTH + 1].valid:
                neighbours.append(index - n * AStar.WIDTH + 1)
        if self.nodes[index].y < AStar.HEIGHT - n - 1:
            if self.nodes[index + n * AStar.WIDTH].valid:
                neighbours.append(index + n * AStar.WIDTH)
            if self.nodes[index].x < AStar.WIDTH - n + 1 and self.nodes[index + n * AStar.WIDTH + 1].valid:
                neighbours.append(index + n * AStar.WIDTH + 1)
            if self.nodes[index].x > 1 and self.nodes[index + n * AStar.WIDTH - 1].valid:
                neighbours.append(index + n * AStar.WIDTH - 1)
        if self.nodes[index].x > n:
            if self.nodes[index - n].valid:
                neighbours.append(index - n)
            if self.nodes[index].y < AStar.HEIGHT - n + 1 and self.nodes[index - n + AStar.WIDTH].valid:
                neighbours.append(index - n + AStar.WIDTH)
            if self.nodes[index].y > 1 and self.nodes[index - n - AStar.WIDTH].valid:
                neighbours.append(index - n - AStar.WIDTH)
        if self.nodes[index].x < AStar.WIDTH - n - 1:
            if self.nodes[index + n].valid:
                neighbours.append(index + n)
            if self.nodes[index].y < AStar.HEIGHT - n + 1 and self.nodes[index + n + AStar.WIDTH].valid:
                neighbours.append(index + n + AStar.WIDTH)
            if self.nodes[index].y > 1 and self.nodes[index + n - AStar.WIDTH].valid:
                neighbours.append(index + n - AStar.WIDTH)
        return neighbours

    def generate_nodes(self):
        self.nodes = []
        for j in range(AStar.SIZE):
            self.nodes.append(AStar.Node(j))
        for i in range(AStar.SIZE):
            if self.map[i] == '0':
                self.nodes[i].valid = self.nodes[i].valid and True
            else:
                self.nodes[i].valid = False
                if i % AStar.WIDTH > 0:
                    self.nodes[i - 1].valid = False
                    if i // AStar.WIDTH > 0:
                        self.nodes[i - AStar.WIDTH - 1].valid = False
                    if i // AStar.WIDTH < AStar.HEIGHT - 1:
                        self.nodes[i + AStar.WIDTH - 1].valid = False
                if i % AStar.WIDTH < AStar.WIDTH - 1:
                    self.nodes[i + 1].valid = False
                    if i // AStar.WIDTH > 0:
                        self.nodes[i - AStar.WIDTH + 1].valid = False
                    if i // AStar.WIDTH < AStar.HEIGHT - 1:
                        self.nodes[i + AStar.WIDTH + 1].valid = False
                if i // AStar.WIDTH > 0:
                    self.nodes[i - AStar.WIDTH].valid = False
                if i // AStar.WIDTH < AStar.HEIGHT - 1:
                    self.nodes[i + AStar.WIDTH].valid = False
        '''
        map = ""
        for j in range(AStar.SIZE):
            if self.nodes[j].valid:
                map += '0'
            else:
                map += '1'
        i = AStar.SIZE
        while i > 0:
            print(map[i - AStar.WIDTH:i])
            i -= AStar.WIDTH
        '''

    def check_node(self, index):
        ''' Check if node can be accessed by 3x3 robot '''
        i = index
        if (not (self.nodes[i].valid)):
		# i % AStar.WIDTH > 0 and not (self.nodes[i - 1].valid) or 
		# i % AStar.WIDTH > 0 and i // AStar.WIDTH > 0 and not (
		# self.nodes[i - AStar.WIDTH - 1].valid) or
		# i % AStar.WIDTH > 0 and i // AStar.WIDTH < AStar.HEIGHT - 1 and not (
		# self.nodes[i + AStar.WIDTH - 1].valid) or
		# i % AStar.WIDTH < AStar.WIDTH - 1 and not (self.nodes[i + 1].valid) or
		# i % AStar.WIDTH < AStar.WIDTH - 1 and i // AStar.WIDTH > 0 and not (
		# self.nodes[i - AStar.WIDTH + 1].valid) or
		# i % AStar.WIDTH < AStar.WIDTH - 1 and i // AStar.WIDTH < AStar.HEIGHT - 1 and not (
		# self.nodes[i + AStar.WIDTH + 1].valid) or
		# i // AStar.WIDTH > 0 and not (self.nodes[i - AStar.WIDTH].valid) or
		# i // AStar.WIDTH < AStar.HEIGHT - 1 and not (self.nodes[i + AStar.WIDTH].valid)):
            return False
        return True

    def generate_shortest_path(self, start, goal):
        path = []
        x = goal
        while (x != start):
            path.append(x)
            x = self.nodes[x].parent.index
        path.append(x)
        path.reverse()
        return path

    def generate_directions(self):
        ''' Generates list of directions for shortest path to goal.
            left = turn left, right = turn right, forward = move forward '''
        self.directions = []
        self.n_turns = 0
        self.total_cost = 0
        direction = self.dir
        for i in range(len(self.shortest_path) - 1):
            cur = self.nodes[self.shortest_path[i]]
            nxt = self.nodes[self.shortest_path[i + 1]]
            new_dir = self.calc_new_dir(nxt, cur)
            if new_dir == direction:
                self.directions.append(Robot.ArduinoCommand.Forward.value)
            elif (direction + 1) % 4 == new_dir:
                self.directions.append(Robot.ArduinoCommand.Rotate_Right.value)
                self.directions.append(Robot.ArduinoCommand.Forward.value)
                self.n_turns += 1
                self.total_cost += AStar.TURN_COST
            elif (direction - 1) % 4 == new_dir:
                self.directions.append(Robot.ArduinoCommand.Rotate_Left.value)
                self.directions.append(Robot.ArduinoCommand.Forward.value)
                self.n_turns += 1
                self.total_cost += AStar.TURN_COST
            else:
                self.directions.append(Robot.ArduinoCommand.Rotate_180.value)
                self.directions.append(Robot.ArduinoCommand.Forward.value)
                self.n_turns += 1
                self.total_cost += AStar.TURN_COST
            self.total_cost += AStar.MVMT_COST
            direction = new_dir
        self.final_orientation = Robot.Orientation(new_dir)

    def print_map(self):
        map = ""
        for i in range(AStar.SIZE):
            if i == self.start:
                map += 'S'
            elif i == self.goal:
                map += 'G'
            elif self.waypoint and i == self.waypoint:
                map += 'W'
            elif i in self.shortest_path:
                map += '*'
            else:
                map += self.map[i]
        i = AStar.SIZE
        while i > 0:
            print(map[i - AStar.WIDTH:i])
            i -= AStar.WIDTH
        print("--Free space: 0, Obstacle: 1, Unknown: 2--")
        print("--Start: S, Goal: G, Waypoint: W, Path : *--\n")

    def to_arduino_command(self, directions):
        ''' Converts directions to arduino-readable mode, i.e. "ar#xxxxx" '''
        arduino_commands = [Robot.ArduinoCommand.AR.value]
        arduino_commands.append("q") # FOR FASTEST PATH
        forward_count = 0
        for d in directions:
            if d != Robot.ArduinoCommand.Forward.value:
                if forward_count > 0:
                    arduino_commands.append(Robot.ArduinoCommand.Forward.value)
                    arduino_commands.append(str(forward_count))
                    forward_count = 0
                arduino_commands.append(d)
            else:
                forward_count += 1
        if forward_count > 0:
            arduino_commands.append(Robot.ArduinoCommand.Forward.value)
            arduino_commands.append(str(forward_count))
        arduino_commands.append(Robot.ArduinoCommand.End.value)
        return ''.join(arduino_commands)


# mh = mapHandler()
# map = mh.read_arena_file('sampleArena/arena3.txt')
# AStar(218, 16, map, direction=3).main()

