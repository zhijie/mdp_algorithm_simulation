#!/usr/bin/python3

import tkinter
import threading
from tkinter import *
from tkinter import filedialog
from mapHandler import mapHandler
from robot import *
from astar import *
import time




class MainUI(threading.Thread):
    GRID_EDGE_SIZE = 27
    GRID_BOTTOM_MOST = 560
    MARGIN_LEFT = 150
    MARGIN_RIGHT = 50
    MARGIN_TOP = 50
    MARGIN_BOTTOM = 50

    COLOR_UNEXPLORED = "#DDDDDD"
    COLOR_OBSTACLE = "#333333"
    COLOR_EXPLORED = "#FFFFFF"
    COLOR_GOAL = "#D4AF37"
    COLOR_START = "#009900"
    COLOR_ROBOT = "#0000ff"
    COLOR_DIRECTION = "#ffff00"


    arenaMap = None
    robot = None
    root = None
    canvas = None
    mh = None

    exploration_start = False
    start_fastest_path = False
    ended = False

    current_index = None
    current_orientation = None

    # Entry
    entry_exploration_target = None
    entry_step_per_second = None
    entry_time_limited = None

    rectangles = [[None for x in range(0, mapHandler.WIDTH)] for y in range(0, mapHandler.HEIGHT)]
    obstacleRectangles = [[None for x in range(0, mapHandler.WIDTH)] for y in range(0, mapHandler.HEIGHT)]
    startExploration = False

    def __init__(self):
        threading.Thread.__init__(self)
        self.arenaMap = list(mapHandler.read_arena_file("sampleArena/emptyArena.txt"))
        self.mh = mapHandler()
        self.robot = Robot(Robot.Orientation.North)

    def on_grid_click(self, event):
        mouseX = event.x
        mouseY = event.y

        posX = int((mouseX - self.MARGIN_LEFT) / self.GRID_EDGE_SIZE)
        posY = int((self.GRID_BOTTOM_MOST - mouseY) / self.GRID_EDGE_SIZE)

        print("x: " + str(posX) + "y:" + str(posY))
        if self.arenaMap[mapHandler.calc_index(posX, posY)] == self.mh.is_explored:
            self.arenaMap[mapHandler.calc_index(posX, posY)] = self.mh.is_obstacle
            self.canvas.itemconfig(self.obstacleRectangles[posY][posX], fill=self.COLOR_OBSTACLE)

        elif self.arenaMap[mapHandler.calc_index(posX, posY)] == self.mh.is_obstacle:
            self.canvas.itemconfig(self.obstacleRectangles[posY][posX], fill=self.COLOR_EXPLORED)
            self.arenaMap[mapHandler.calc_index(posX, posY)] = self.mh.is_explored

    def repaint_arena(self):

        for y in range(0, 20):
            for x in range(0, 15):
                if self.arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_obstacle:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_OBSTACLE)

                elif self.arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_explored:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_EXPLORED)

                elif self.arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_unexplored:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_UNEXPLORED)

                elif self.arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_goal:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_GOAL)

                elif self.arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_start:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_START)

                    # self.drawObstacleGrid(posX, posY)

    def paint_arena_explore(self, arenaMap, location, orientation):

        robot = [location - 1, location + 1,
                 location + mapHandler.WIDTH, location + mapHandler.WIDTH + 1,
                 location + mapHandler.WIDTH - 1, location,
                 location - mapHandler.WIDTH, location - mapHandler.WIDTH + 1,
                 location - mapHandler.WIDTH - 1]

        robot_facing = []
        if orientation == Robot.Orientation.North.value:
            robot_facing.append(location + 15)
        elif orientation == Robot.Orientation.South.value:
            robot_facing.append(location - 15)
        elif orientation == Robot.Orientation.East.value:
            robot_facing.append(location + 1)
        elif orientation == Robot.Orientation.West.value:
            robot_facing.append(location - 1)

        start = []
        goal = []

        for y in range(0, 3):
            for x in range(0, 3):
                start.append(mapHandler.calc_index(x,y))

        for y in range(17, 20):
            for x in range(12, 15):
                goal.append(mapHandler.calc_index(x,y))

        for y in range(0, 20):
            for x in range(0, 15):

                if mapHandler.calc_index(x, y) in robot_facing:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_DIRECTION)

                elif mapHandler.calc_index(x, y) in robot:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_ROBOT)

                elif mapHandler.calc_index(x, y) in goal:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_GOAL)

                elif mapHandler.calc_index(x, y) in start:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_START)

                elif arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_obstacle:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_OBSTACLE)

                elif arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_explored:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_EXPLORED)

                elif arenaMap[mapHandler.calc_index(x, y)] == self.mh.is_unexplored:
                    self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_UNEXPLORED)

    '''
    Set variables for the explore program to use 
    Prepare the arena for exploration
    e = Exploration(start_position=16, time_limited_exploration=0,
                coverage_limited_exploration=0, steps_per_second=5,
                obstacle_map_path="sampleArena/arena2.txt") 
    '''
    def start_exploration(self):

        self.set_entry_with_default_values()
        self.exploration_start = True


    def set_entry_with_default_values(self):
        if len(self.entry_exploration_target.get()) == 0:
            self.set_text(self.entry_exploration_target, "0")

        if len(self.entry_step_per_second.get()) == 0:
            self.set_text(self.entry_step_per_second, "10")

        if len(self.entry_time_limited.get())  == 0:
            self.set_text(self.entry_time_limited, "0")

    def set_text(self, entry, text):
        entry.delete(0,END)
        entry.insert(0,text)

    def save_arena_mdf(self):
        file_mdf1 = "mdf1.txt"
        file_mdf2 = "mdf2.txt"

        mdf1 = self.mh.generate_mfd1(self.arenaMap)
        mdf2 = self.mh.generate_mfd2(self.arenaMap)

        with open(file_mdf1, 'w') as my_mdf_file1:
            my_mdf_file1.write(mdf1)
            my_mdf_file1.close()

        with open(file_mdf2, 'w') as my_mdf_file2:
            my_mdf_file2.write(mdf2)
            my_mdf_file2.close()

        print("MDF1: " + mdf1)
        print("MDF2: " + mdf2)
        print("File Saved! ")

    def fastest_path(self):
        self.set_entry_with_default_values()
        self.start_fastest_path = True

    def load_map(self):

        file = filedialog.askopenfilename()
        self.arenaMap = list(mapHandler.read_arena_file(file))
        self.repaint_arena()
        self.paint_start_goal()

    def save_map(self):
        file = filedialog.asksaveasfilename()

        with open(file, 'w') as my_mdf_file1:
            arena = mapHandler.flip_map(''.join(self.arenaMap))
            my_mdf_file1.write(arena)
            my_mdf_file1.close()

    def load_mdf_map(self):
        file_mdf1 = "mdf1.txt"
        file_mdf2 = "mdf2.txt"

        self.arenaMap = list(self.mh.read_mdf_file(file_mdf1, file_mdf2))
        self.repaint_arena()
        self.paint_start_goal()
        print("Completed")

    def reset_map(self):
        self.arenaMap = list(mapHandler.read_arena_file("sampleArena/emptyArena.txt"))
        self.repaint_arena()
        self.paint_start_goal()
        print("Reset Map Completed")

    def paint_start_goal(self):
        # Paint Goal color
        for y in range(17, 20):
            for x in range(12, 15):
                self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_GOAL)

        # Paint Start Color
        for y in range(0, 3):
            for x in range(0, 3):
                self.canvas.itemconfig(self.obstacleRectangles[y][x], fill=self.COLOR_START)

    def run(self):
        self.root = tkinter.Tk()
        self.root.protocol("WM_DELETE_WINDOW", self.exit)
        self.root.wm_state('zoomed')
        self.raise_above_all(self.root)

        row_1 = tkinter.Frame()
        row_1.pack(fill=BOTH, expand=TRUE)

        self.canvas = tkinter.Canvas(row_1, width=self.root.winfo_screenwidth() / 2, height=600, bg="white")
        self.canvas.pack()

        # Define Maze
        self.generate_ui_components()
        # self.root.mainloop()

    def raise_above_all(self, window):
        window.lift()
        window.attributes('-topmost', True)
        window.after_idle(window.call, 'wm', 'attributes', '.', '-topmost', False)
        window.focus_force()

        # window.attributes('-topmost', False)

    def generate_ui_components(self):

        # Initialize rectangles
        for i in range(0, 20):
            for j in range(0, 15):
                self.obstacleRectangles[i][j] = self.canvas.create_rectangle \
                                    (self.MARGIN_LEFT + j * self.GRID_EDGE_SIZE,
                                     self.GRID_BOTTOM_MOST - i * self.GRID_EDGE_SIZE,
                                     self.MARGIN_LEFT + j * self.GRID_EDGE_SIZE + self.GRID_EDGE_SIZE,
                                     self.GRID_BOTTOM_MOST - i * self.GRID_EDGE_SIZE - self.GRID_EDGE_SIZE,
                                     outline="#000000",
                                     fill="#FFFFFF")
                self.canvas.tag_bind(self.obstacleRectangles[i][j], '<ButtonPress-1>', self.on_grid_click)

        self.paint_start_goal()

        row_2 = tkinter.Frame()
        row_2.pack(fill=tkinter.X)

        self.button_save_arena_mdf = tkinter.Button(row_2, text="Save Arena (MDF)!", command=self.save_arena_mdf)
        self.button_save_arena_mdf.pack(side=tkinter.LEFT)
        self.button_load_arena_mdf = tkinter.Button(row_2, text="Load Arena (MDF)!", command=self.load_mdf_map)
        self.button_load_arena_mdf.pack(side=tkinter.LEFT)
        self.button_explore = tkinter.Button(row_2, text="Explore", command=self.start_exploration)
        self.button_explore.pack(side=tkinter.LEFT)
        self.button_fastest_path = tkinter.Button(row_2, text="Fastest Path", command=self.fastest_path)
        self.button_fastest_path.pack(side=tkinter.LEFT)
        self.button_save_arena = tkinter.Button(row_2, text="Save Map", command=self.save_map)
        self.button_save_arena.pack(side=tkinter.LEFT)
        self.button_load_arena = tkinter.Button(row_2, text="Load Map", command=self.load_map)
        self.button_load_arena.pack(side=tkinter.LEFT)
        self.button_reset_arena = tkinter.Button(row_2, text="Reset Map", command=self.reset_map)
        self.button_reset_arena.pack(side=tkinter.LEFT)

        row_3 = tkinter.Frame()
        row_3.pack(fill=tkinter.X)

        label_exploration_target = tkinter.Label(row_3, text="Exploration Target (%): ", width=30)
        label_exploration_target.pack(side=tkinter.LEFT)
        self.entry_exploration_target = tkinter.Entry(row_3)
        self.entry_exploration_target.pack(side=tkinter.LEFT, fill=tkinter.X, expand=True)

        row_4 = tkinter.Frame()
        row_4.pack(fill=tkinter.X)

        label_step_per_second = tkinter.Label(row_4, text="Steps per second: ", width=30)
        label_step_per_second.pack(side=tkinter.LEFT)
        self.entry_step_per_second = tkinter.Entry(row_4)
        self.entry_step_per_second.pack(side=tkinter.LEFT, fill=tkinter.X, expand=True)

        row_5 = tkinter.Frame()
        row_5.pack(fill=tkinter.X)

        label_time_limited = tkinter.Label(row_5, text="Time Limited Exploration (Sec): ", width=30)
        label_time_limited.pack(side=tkinter.LEFT)
        self.entry_time_limited = tkinter.Entry(row_5)
        self.entry_time_limited.pack(side=tkinter.LEFT, fill=tkinter.X, expand=True)

    def exit(self):
        self.ended = True
        self.root.quit()
