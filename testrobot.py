from robot import *
from mapHandler import *



def main():
    current_map = mapHandler.read_arena_file("sampleArena/testsensor.txt")
    r = Robot()
    current_map = list(current_map)
    mapHandler.print_flipped_grid(current_map)
    print(r.orientation)
    updated_map = r.get_sensor_data(current_map,65)
    mapHandler.print_flipped_grid(updated_map)

main()
