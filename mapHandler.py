#!/usr/bin
class mapHandler:
    WIDTH = 15
    HEIGHT = 20

    def __init__(self):
        self.WIDTH = 15
        self.HEIGHT = 20
        self.SIZE = self.WIDTH * self.HEIGHT
        self.is_obstacle = "1"
        self.is_unexplored = "2"
        self.is_explored = "0"
        self.is_goal = "4"
        self.is_start = "3"
        self.check_virtual_west = [13, -2, -17]
        self.check_right_grid = [17, 2, -13]
        self.check_front_grid = [29, 30, 31]
        self.check_bottom_grid = [-31, -30, -29]

    # Part 1 Explored/Unexplored State
    @staticmethod
    def generate_mfd1(bin_map):
        bin_map = ''.join(bin_map)

        # Explored = 0, Unexplored = 1, Obstacle = 2

        bin_map = bin_map.replace("0", "1")  # Convert all non obstacle to be explored
        bin_map = bin_map.replace("2", "0") # Convert all unexplored to unexplored in MDF format
        bin_map = bin_map.replace("3", "1")
        bin_map = bin_map.replace("4", "1")
        padded_map = "11" + bin_map + "11"
        p1_map = hex(int(padded_map, 2))
        p1_map = p1_map[2:].upper()  # Convert to uppercase
        return p1_map

    # Part 2 Only cells that were previously marked explored  in P1 will be represented here. Pad 0 to multiple of 8 bits.
    @staticmethod
    def generate_mfd2(bin_map):
        bin_map = ''.join(bin_map)

        bin_map = bin_map.replace("3", "0") # GOAL
        bin_map = bin_map.replace("4", "0") # Start
        bin_map = bin_map.replace('2', '')
        num_to_pad = 4 - (len(bin_map) % 4)
        bin_map = bin_map + ("0" * num_to_pad)

        num_of_hex_digits = len(bin_map) // 4
        mfd2 = hex(int(bin_map, 2))[2:].upper()
        mdf2 = str(mfd2.zfill(num_of_hex_digits))
        return mdf2

    # To cater for the map descriptor formatting
    @staticmethod
    def flip_map(bin_map):
        i = 300
        flipped_bin_map = ""
        while i > 0:
            flipped_bin_map += bin_map[i - 15:i]
            i -= 15
        return flipped_bin_map

    # Used to flip the pdf grids for testing purposes
    @staticmethod
    def print_flipped_grid(bin_map):
        bin_map = ''.join(bin_map)
        i = 300
        while i > 0:
            print(bin_map[i - 15:i])
            i -= 15

    @staticmethod
    def print_viewing_grid(bin_map):
        bin_map = ''.join(bin_map)
        i = 0
        while i < 300:
            print(bin_map[i:i + 15])
            i += 15

    @staticmethod
    def read_arena_file(filename):
        with open(filename, 'r') as my_file:
            arena = my_file.read()
            # Clean lines
            arena = arena.replace('\n', '')
            arena = arena.replace(' ', '')
            arena = arena.replace('\r', '')

        # Need to reverse the lines to make start at 16
        flipped_arena = ""
        i = 300
        while i > 0:
            flipped_arena += arena[i - 15:i]
            i -= 15

        return flipped_arena

    @staticmethod
    def read_mdf_file(mdf1, mdf2):
        new_arena = "2" * 300
        new_arena = list(new_arena)

        with open(mdf1, 'r') as mdf1_file:
            mdf_arena1 = mdf1_file.read()
            mdf_arena1 = mdf_arena1.replace('\n', '')
            mdf_arena1 = bin(int(mdf_arena1, 16))[2:].zfill(len(new_arena))
            mdf_arena1 = mdf_arena1[2:len(mdf_arena1)-2]

        count = 0
        for i in range (len(new_arena)):

            if mdf_arena1[i] == "1":
                new_arena[i] = 0
                count += 1

        with open(mdf2, 'r') as mdf2_file:
            mdf_arena2 = mdf2_file.read()
            mdf_arena2 = mdf_arena2.replace('\n', '')
            mdf_arena2 = bin( int(mdf_arena2, 16))[2:].zfill(len(mdf_arena2)*4)

        count = 0
        for i in range(len(new_arena)):
            if new_arena[i] == "2":
                continue
            new_arena[i] = mdf_arena2[count]
            count += 1

        new_arena = ''.join(new_arena)
        # new_arena = mapHandler.flip_map(new_arena)
        # mapHandler.print_viewing_grid(new_arena)
        return new_arena


    @staticmethod
    def calculate_x_position(position):
        return int(position % 15)

    @staticmethod
    def calculate_y_position(position):
        return int(position / 15)

    @staticmethod
    def calc_index(x,y):
        return (y*mapHandler.WIDTH) + x

# ---------------------------------------------------------------------------------------------------------------------
# Testing Codes
# Test for Sample Arena
'''
bin_map = mapHandler.read_arena_file("sampleArena/sample_arena1.txt")
print("Part 1: " + mapHandler.generate_mfd1(bin_map))
bin_map2 = mapHandler.generate_mfd2(bin_map)
print("Part 2: " + bin_map2)

# Test for Converting MDF to Arena
new_arena = mapHandler.read_mdf_file("sampleArena/sample_arena_1_mdf1.txt", "sampleArena/sample_arena_1_mdf2.txt")
mapHandler.print_viewing_grid(new_arena)
'''
# Sample Arena 1 Results
# FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
# 000000000400000001C800000000000700000000800000001F80000700000000020000000000

# Sample Arena 2 Results
# FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
# 000000000080010042038400000000000000010C000000000000021F84000800000000000400

